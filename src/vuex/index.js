import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

const state = {
  data: ''
}

const mutations = {
  GETMSG: function (state, arr) {
    state.data = arr
  }
}

const actions = {
  getMsg: function ({commit}) {
    axios.get('https://cur1ous.com/live/anchor/?name=%E6%B5%81%E6%B0%93%E5%85%94')
     .then(function (res) {
       // var resData = res.data.data
       console.log(res)
       // commit('GETMSG', resData)
     })
  }
}

export default new Vuex.Store({
  state,
  mutations,
  actions
})
