import Vue from 'vue'
import Router from 'vue-router'
import Container from '../components/Main/container'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      redirect: '/all'
    },
    {
      path: '/all',
      component: Container
    }
  ]
})
